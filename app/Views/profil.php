<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>
<div id="content" role="main">
    <div class="page-header dark larger larger-desc">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?=$profil->judul?></h1>
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-header -->

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div style="color:#333;">
                <?php if ($profil->gambar != null) {?>
                <img class="img-responsive" src="https://bkpp.gorontalokota.go.id/storage/profil_tupoksi/<?=$profil->gambar?>">
                <?php }?>
                <p><?=$profil->isi?></p>
            </div>
             </div>
        </div>
    </div>
</div>
<?=$this->endSection('content');?>