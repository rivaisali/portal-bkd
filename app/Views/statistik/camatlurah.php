<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>
<script src="<?=base_url('assets/charts/amcharts/plugins/export/export.min.js');?>"></script>
<link rel="stylesheet" href='<?=base_url('assets/charts/amcharts/plugins/export/export.css')?>' type='text/css' media='all'/>
<script src="<?=base_url('assets/charts/amcharts/amcharts.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/charts/amcharts/pie.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/charts/amcharts/serial.js')?>" type="text/javascript"></script>

<div id="content" role="main">
<br><br>

<div class="container">
<div class="row">
<?php
if ($type == "pie") {?>
<script>
            var chart;
            var legend;
            var chartData = [
                 {
                    "data": "LURAH",
                    "value": <?=$lurah->count?>
                 },
                {
                    "data": "CAMAT",
                    "value": <?=$camat->count?> },
                          ];

            AmCharts.ready(function () {
                // PIE CHART
                chart = new AmCharts.AmPieChart();
                chart.dataProvider = chartData;
                chart.titleField = "data";
                chart.valueField = "value";
                chart.outlineColor = "#FFFFFF";
                chart.outlineAlpha = 0.8;
                chart.outlineThickness = 2;
                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                // this makes the chart 3D
                chart.depth3D = 15;
                chart.angle = 30;
                // WRITE
                chart.write("chartdiv");

            });
        </script>
<?php } else {?>
<script>
var chart;

var chartData = [{
        "data": "LURAH",
        "value": <?=$lurah->count?>,
        "color": "#F8FF01"
    },
    {
        "data": "CAMAT",
        "value": <?=$camat->count?>,
        "color": "#FF9E01"
    },
];


AmCharts.ready(function () {
    // SERIAL CHART
    chart = new AmCharts.AmSerialChart();
    chart.dataProvider = chartData;
    chart.categoryField = "data";
    chart.startDuration = 1;
    chart.depth3D = 50;
    chart.angle = 33;
    chart.marginRight = -45;

    // AXES
    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.gridAlpha = 33;
    categoryAxis.axisAlpha = 0;
    categoryAxis.gridPosition = "start";

    // value
    var valueAxis = new AmCharts.ValueAxis();
    valueAxis.axisAlpha = 0;
    valueAxis.gridAlpha = 1;
    chart.addValueAxis(valueAxis);

    // GRAPH
    var graph = new AmCharts.AmGraph();
    graph.valueField = "value";
    graph.colorField = "color";
    graph.balloonText = "<b>[[category]]: [[value]]</b>";
    graph.type = "column";
    graph.lineAlpha = 0.5;
    graph.lineColor = "#FFFFFF";
    graph.topRadius = 1;
    graph.fillAlphas = 0.9;
    chart.addGraph(graph);

    // CURSOR
    var chartCursor = new AmCharts.ChartCursor();
    chartCursor.cursorAlpha = 0;
    chartCursor.zoomable = false;
    chartCursor.categoryBalloonEnabled = false;
    chartCursor.valueLineEnabled = true;
    chartCursor.valueLineBalloonEnabled = true;
    chartCursor.valueLineAlpha = 1;
    chart.addChartCursor(chartCursor);

    chart.creditsPosition = "top-right";

    // WRITE
    chart.write("chartdiv");
});
</script>

        <?php }?>


            <div class="panel panel-success">
                <div class="panel-heading" style="padding:15px 0px 0px 15px">
                    <h2>Rekapitulasi Jumlah Pegawai Berdasarkan Camat dan Lurah</h2>
                </div>

                <div class="panel-body isi">


                                    <div class="col-md-5">


                        <p>Rekapitulasi Jumlah Pegawai Negeri Sipil (PNS & CPNS) di Lingkungan Pemerintah Kota Gorontalo Berdasarkan Jenis Kelamin</p>

                       <table class="table table-bordered table-hover table-striped" width="100%">
                        <thead class="bordered-darkorange">
                        <tr>
                            <th style="text-align:center; width:40px;">No</th>
                            <th class="text-center">Keterangan</th>
                            <th class="text-center">Jumlah</th>
                        </tr>
                        </thead>
                        <tbody>
                                                <tr class="odd gradeX">
                            <td class="text-center">1</td>
                            <td>CAMAT</td>
                            <td><?=$camat->count?></td>
                        </tr>
                                                <tr class="odd gradeX">
                            <td class="text-center">2</td>
                            <td>LURAH</td>
                            <td><?=$lurah->count?>   </td>
                        </tr>
                        						<tr style="font-weight:bold">
							<td colspan="2" class="text-center">JUMLAH</td>
							<td><?=($camat->count) + ($lurah->count)?>   </td>
							</tr>                        </tbody>
                        </table>

                    </div>

                    <div class="col-md-7">

                        <div class="link_grafik" id="chartdiv" style="width: 100%; height: 400px;"></div>
                                            </div>


                </div>

<div class="panel-footer isi">
	<div class="row">
		<div class="col-md-7 text-left">
			<table>
				<tr>
					<td style="width:50px"><i class="fa fa-3x fa-info-circle" style=" vertical-align: middle;color:#7f8c8d"></i></td>
					<td><div style="color:#7f8c8d">Data ini bersifat statis dan dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya.</div></td>
				</tr>
			</table>
		</div>
        <div class="col-md-5 text-right">
			<a href="<?=base_url('statistik/jabatan-camat-lurah?type=pie')?>" class="btn btn-primary btn-sm"><i class="fa fa-pie-chart"></i> Grafik Lingkaran</a>
			<a href="<?=base_url('statistik/jabatan-camat-lurah')?>" class="btn btn-success btn-sm"><i class="fa fa-bar-chart"></i> Grafik Batang</a>
			</div>
	</div>
</div>
</div>
</div>



</div>


 </div><!-- End .row -->
</div><!-- End .container -->
<?=$this->endSection('content');?>