<?php
namespace App\Controllers;

use App\Models\CustomModel;
use App\Models\SimpegModel;

class Main extends BaseController
{

    protected $model;
    protected $simpeg;

    public function __construct()
    {
        helper('cookie');

        $this->model = new CustomModel();
        $this->simpeg = new SimpegModel();

    }

    public function index()
    {
        $data['post'] = $this->model->select_data("post", "getResult", false, null, ["created_at", "DESC"], 5);
        $data['terbaru'] = $this->model->select_data("post", "getResult", ["kategori" => "berita"], null, ["created_at", "DESC"], 8);
        $data['popup'] = $this->model->select_data("post", "getRow", ["kategori" => "pengumuman"], null, ["created_at", "DESC"], 1);
        $data['aplikasi'] = $this->model->select_data("link_app", "getResult");
        $data['status'] = $this->simpeg->select_data_join("r_dip.statuspns,t_statuspns.nmstatuspns", "r_dip", "getResult", [
            [
                "table" => "t_statuspns",
                "cond" => "t_statuspns.idstatuspns = r_dip.statuspns",
                "type" => "",
            ],

        ], false, null, null, ["t_statuspns.idstatuspns"]);

        return view('home', $data);

    }

    public function post($kategori)
    {
        $data['post'] = $this->model->select_data("post", "getResult", ["kategori" => $kategori], null, ["created_at", "DESC"], 8);
        return view('post', $data);
    }

    public function filter()
    {
        $query = $this->request->getPost('query');
        return redirect()->to('result/' . $query);
    }

    public function result($query)
    {
        $data['title'] = $query;
        $data['filter'] = $this->model->select_data("post", "getResult", false, ["judul" => $query]);

        return view('filter', $data);

    }

    public function post_detail($slug)
    {
        $data['post'] = $this->model->select_data("post", "getRow", ["slug" => $slug]);
        $data['lainnya'] = $this->model->select_data("post", "getResult", ["kategori" => "berita"], null, ["created_at", "DESC"], 10);
        return view('detail_post', $data);
    }

    public function statistik($kategori)
    {

        $uri = service('uri');
        if ($kategori == "jenis_kelamin") {
            $data['type'] = substr($uri->getQuery(['only' => ['type']]), 5);
            $data['lakilaki'] = $this->simpeg->select_data_count("r_dip", "getRow", ["jk" => 1, "statuspns" => 1]);
            $data['perempuan'] = $this->simpeg->select_data_count("r_dip", "getRow", ["jk" => 2, "statuspns" => 1]);
            return view('statistik/jenis_kelamin', $data);

        }

        if ($kategori == "usia") {
            $data['type'] = substr($uri->getQuery(['only' => ['type']]), 5);
            $data['usia25'] = $this->simpeg->select_data_count("r_dip", "getResult", ["(YEAR(CURDATE())-YEAR(tgllahir))<" => 25, "statuspns" => 1]);
            $data['usia25sampai30'] = $this->simpeg->select_data_count("r_dip", "getResult", ["(YEAR(CURDATE())-YEAR(tgllahir))>=" => 25, "(YEAR(CURDATE())-YEAR(tgllahir))<=" => 30, "statuspns" => 1]);
            $data['usia31sampai36'] = $this->simpeg->select_data_count("r_dip", "getResult", ["(YEAR(CURDATE())-YEAR(tgllahir))>=" => 31, "(YEAR(CURDATE())-YEAR(tgllahir))<=" => 36, "statuspns" => 1]);
            $data['usia37sampai42'] = $this->simpeg->select_data_count("r_dip", "getResult", ["(YEAR(CURDATE())-YEAR(tgllahir))>=" => 37, "(YEAR(CURDATE())-YEAR(tgllahir))<=" => 42, "statuspns" => 1]);
            $data['usia43sampai48'] = $this->simpeg->select_data_count("r_dip", "getResult", ["(YEAR(CURDATE())-YEAR(tgllahir))>=" => 43, "(YEAR(CURDATE())-YEAR(tgllahir))<=" => 48, "statuspns" => 1]);
            $data['usia49sampai55'] = $this->simpeg->select_data_count("r_dip", "getResult", ["(YEAR(CURDATE())-YEAR(tgllahir))>=" => 49, "(YEAR(CURDATE())-YEAR(tgllahir))<=" => 55, "statuspns" => 1]);
            $data['usia55'] = $this->simpeg->select_data_count("r_dip", "getResult", ["(YEAR(CURDATE())-YEAR(tgllahir))>" => 55, "statuspns" => 1]);

            return view('statistik/usia', $data);

        }

        if ($kategori == "jabcamatlurah") {
            $data['type'] = substr($uri->getQuery(['only' => ['type']]), 5);

            $data['lurah'] = $this->simpeg->select_data_count("v_api", "getRow", ["jabatann" => "LURAH", "statuspns" => 1]);
            $data['camat'] = $this->simpeg->select_data_count("v_api", "getRow", ["jabatann" => "CAMAT", "statuspns" => 1]);

            return view('statistik/camatlurah', $data);

        }

        if ($kategori == "status") {
            $data['type'] = substr($uri->getQuery(['only' => ['type']]), 5);

            $data['status'] = $this->simpeg->select_data_join("r_dip.statuspns,t_statuspns.nmstatuspns", "r_dip", "getResult", [
                [
                    "table" => "t_statuspns",
                    "cond" => "t_statuspns.idstatuspns = r_dip.statuspns",
                    "type" => "",
                ],

            ], false, null, null, ["t_statuspns.idstatuspns"]);

            return view('statistik/status', $data);

        }

        if ($kategori == "eselon") {
            $data['type'] = substr($uri->getQuery(['only' => ['type']]), 5);

            $data['eselon'] = $this->simpeg->select_data_join("v_dnp.eselon,t_eselon.nmeselon", "v_dnp", "getResult", [
                [
                    "table" => "t_eselon",
                    "cond" => "t_eselon.ideselon = v_dnp.eselon",
                    "type" => "",
                ],

            ], false, null, null, ["t_eselon.ideselon"]);

            return view('statistik/eselon', $data);

        }
        if ($kategori == "jabatan") {
            $data['type'] = substr($uri->getQuery(['only' => ['type']]), 5);

            $data['jabatan'] = $this->simpeg->select_data_join("r_jabatan.jabatanjenis,t_jabatanjenis.nmjabatannow", "r_jabatan", "getResult", [
                [
                    "table" => "t_jabatanjenis",
                    "cond" => "t_jabatanjenis.idjabatannow = r_jabatan.jabatanjenis",
                    "type" => "",
                ],

            ], false, null, null, ["r_jabatan.jabatanjenis"]);

            return view('statistik/jabatan', $data);

        }

    }

    public function profilPegawai()
    {
        $filter = $this->request->getPost('key');
        if ($filter) {
            $data['data'] = $this->simpeg->select_data("v_api", "getResult", false, ["nama" => $filter]);
            return view('profil_pegawai', $data);

        } else {
            return view('profil_pegawai');

        }
        // $db = db_connect('simpeg', true);
        // echo $db->getLastQuery();

    }

    public function detailPegawai($nip)
    {
        $data['data'] = $this->simpeg->select_data("v_api", "getRow", ["nip2" => $nip]);
        return view('detail_pegawai', $data);

        // $db = db_connect('simpeg', true);
        // echo $db->getLastQuery();

    }

    public function profil($slug)
    {
        $data['profil'] = $this->model->select_data("profil", "getRow", ["slug" => $slug]);
        return view('profil', $data);

    }

    public function tupoksi($slug)
    {
        $data['tupoksi'] = $this->model->select_data("tupoksi", "getRow", ["slug" => $slug]);
        return view('tupoksi', $data);

    }

    public function subtupoksi($slug)
    {
        $result = $this->model->select_data("tupoksi_sub", "getRow", ["slug" => $slug]);
        if ($result) {
            $data['tupoksi'] = $result;
            return view('tupoksi', $data);
        } else {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

    }

    public function pelayanan($slug)
    {
        $data['pelayanan'] = $this->model->select_data("layanan", "getRow", ["slug" => $slug]);
        return view('pelayanan', $data);

    }

    public function regulasi($slug)
    {
        $id_regulasi = $this->model->select_data("regulasi", "getRow", ["slug" => $slug])->id;
        $data['regulasi'] = $this->model->select_data("regulasi_file", "getResult", ["regulasi_id" => $id_regulasi]);
        return view('regulasi', $data);

    }

    public function kontak()
    {
        $data['pengaturan'] = $this->model->select_data("pengaturan", "getResult");

        return view('kontak', $data);

    }

}
