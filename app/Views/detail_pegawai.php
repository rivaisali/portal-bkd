<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>

<div id="content" role="main">
    <div class="page-header dark larger larger-desc">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Cari Profil ASN</h1>
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <div class="container">
        <div class="row">
            <div class="panel panel-success">
                <div class="panel-heading" style="padding:15px 0px 0px 15px">
                    <h3>Data Detil Pegawai</h3>
                </div>

                <div class="panel-body isi">
                    <div class="col-md-12">
                      <div class="panel-body">
              <div class="row">
                <div align="center"> <img alt="User Pic" height="200" src="http://simpeg.bkpp.gorontalokota.go.id/foto/<?=$data->photo?>">
                </div>
                <br>
                 <table class="table table-bordered table-hover table-striped" width="100%">
                    <tbody>
                      <tr>
                        <td>Nama Lengkap:</td>
                        <td><?=$data->nama?></td>
                      </tr>
                      <tr>
                        <td>TMT CPNS:</td>
                        <td><?=$data->tmtdp?></td>
                      </tr>
                      <tr>
                        <td>TMT PNS:</td>
                        <td><?=$data->tmt?></td>
                      </tr>
                      <tr>
                        <td>Golongan Ruang</td>
                        <td><?=$data->goldesc?></td>
                      </tr>

                      <tr>
                        <td>Pendidikan Terakhir</td>
                        <td><?=$data->nmpendik?></td>
                      </tr>

                      <tr>
                        <td>Jabatan</td>
                        <td><?=$data->jabatann?></td>
                      </tr>

                      <tr>
                        <td>Unit Organisasi</td>
                        <td><?=$data->unorname?></td>
                      </tr>
                      <tr>
                        <td>Kedudukan PNS</td>
                        <td><?php if ($data->statuspns == 1) {
    echo "Aktif";
} else {
    "Tidak Aktif";
}
?></td>
                      </tr>


                    </tbody>
                  </table>

                    </div>



                </div>

                <div class="panel-footer isi">
                    <div class="row">
                        <div class="col-md-7 text-left">
                            <table>
                                <tr>
                                    <td style="width:50px"><i class="fa fa-3x fa-info-circle"
                                            style=" vertical-align: middle;color:#7f8c8d"></i></td>
                                    <td>
                                        Catatan : Jika data anda tidak sesuai, harap hubungi SKPD atau Biro Kepegawaian di instansi saudara dengan membawa dokumen yang otentik.
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>




    </div>


</div><!-- End .row -->
</div><!-- End .container -->
<?=$this->endSection('content');?>