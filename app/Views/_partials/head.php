<meta charset="utf-8">
<title>Selamat Datang di Website BKPP Kota Gorontalo</title>
<meta name="keywords"
    content="BKPP Gorontalo, Website BKPP Gorontalo, BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN KOTA GORONTALO" />
<meta name="description" content="WEBSITE BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN KOTA GORONTALO" />

<meta property="og:url" content="https://bkpp.gorontalokota.go.id" />
<meta property="og:site_name" content="BKPP Kota Gorontalo" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Website Resmi BKPP KOTA GORONTALO" />
<meta property="og:description" content="WEBSITE BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN KOTA GORONTALO" />
<meta property="og:image" content="https://bkpp.gorontalokota.go.id/img/iconapp.png" />

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="BKPP Kota Gorontalo">
<meta name="twitter:title" content="Website Resmi BKPP KOTA GORONTALO">
<meta name="twitter:description" content="WEBSITE BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN KOTA GORONTALO">
<meta name="twitter:creator" content="@bkpp_gorontalo">
<meta name="twitter:image" content="https://bkpp.gorontalokota.go.id/img/iconapp.png">

<meta name="author" content="BKPP KOTA GORONTALO">
<meta name="theme-color" content="#FFC400" />

<!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,300italic,400italic,700italic' rel='stylesheet'
    type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,500,600,700,800,900' rel='stylesheet'
    type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link
    href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,400italic,600italic,700italic,600,800,300,700,800italic'
    rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
<!-- Google Fonts -->

	<link rel="canonical" href="<?php echo site_url() ?>"/>

<link rel="stylesheet" href="<?php echo base_url('assets/main/css/animate.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/main/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/main/css/magnific-popup.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/main/css/style.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/main/css/revslider/revslider-index33.css'); ?>">
<link rel="stylesheet" id="color-scheme" href="<?php echo base_url('assets/main/css/colors/yellow.css'); ?>">

<!-- Favicon and Apple Icons -->
<link rel="icon" type="image/png" href="assets/filemanager/userfiles/image/default_image__final_fw.png">
<link rel="apple-touch-icon" sizes="57x57" href="assets/main/images/icons/faviconx57.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/main/images/icons/faviconx72.png">

<!-- Modernizr -->
<script src="<?php echo base_url('assets/main/js/modernizr.js'); ?>"></script>

<!--- jQuery & Bootstrap JS -->
<script src="<?php echo base_url('assets/main/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/main/js/bootstrap.min.js'); ?>"></script>

<!-- Queryloader -->
<script src="<?php echo base_url('assets/main/js/queryloader2.min.js'); ?>"></script>

<!--Page Related styles-->
<link rel="stylesheet" href="<?php echo base_url('assets/main/css/dataTables.bootstrap.min.css'); ?>">

<style>
    .bg-white {
        background-color: #fff;
    }

    .kotak {
        /*max-width: 1210px;*/
        margin: auto;
    }

    .entry-content {
        color: #000;
    }

    .entry-date,
    .bg-statistik {
        background-color: #009688;
    }

    th,
    td {
        padding: 6px 12px !important;
        color: #000 !important;
    }

    .isi {
        color: #000;
    }

    /* 	.panel-heading {
 		padding: 10px 20px;
 	}	*/
    .dataTables_length {
        display: none;
    }

    .dataTables_filter label,
    .dataTables_length label {
        color: #000 !important;
    }

    .dataTables_filter input {
        float: right !important;
        color: #000 !important;
        border-radius: 5px;
        padding: 4px 6px;
        border: solid thin #EEE;
        background-color: #f5f5f5;
    }

    .dataTables_length select {
        color: #000 !important;
        border-radius: 5px;
        padding: 6px 12px;
        border: solid thin #EEE;
        background-color: #f5f5f5;
    }

    input,
    select,
    textarea,
    .kontak {
        color: #000 !important;
    }

    /*.tp-caption {
 	 	background-color: purple;
 	 	padding: 10px 2%;
 	 	opacity: 0.8;
 	 }*/
    .navbar-nav li {
        font-size: 12px !important;
    }

    .link_grafik a {
        display: none !important;
        font-size: 0;
        color: transparent;
    }
</style>