 <?php
$model = new \App\Models\CustomModel;
$regulasi = $model->select_data("regulasi", "getResult");
$profil = $model->select_data("profil", "getResult");
$berita = $model->select_data("post", "getResult", ["kategori" => "berita"], null, ["created_at", "DESC"], 8);

?>
 <footer id="footer" class="footer-inverse" role="contentinfo">
        <div id="footer-inner"
            style="background-image: url('<?=base_url('assets/images/footer_lodyas.png')?>');">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <div class="corporate-widget">
                                <address>
                                    <h4>Badan Kepegawaian Pendidikan dan Pelatihan Kota Gorontalo</h4>
                                    Jalan 23 Januari No. 184 <br />
                                    <br />
                                    Kelurahan Biawao Kecamatan Kota Selatan,<br />
                                    Kota Gorontalo<br />
                                    Provinsi Gorontalo<br /><br>
                                    <i class="fa fa-university" aria-hidden="true"></i>&nbsp;<a
                                        href="https://bkpp.gorontalokota.go.id">https: //bkpp.gorontalokota.go.id</a><br>
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;<a
                                        href="mailto:bkpp@gorontalokota.go.id">bkpp@gorontalokota.go.id</a><br>
                                    <i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;(0435) 821073<br>
                                    <i class="fa fa-fax" aria-hidden="true"></i>&nbsp;(0435) 821073<br>
                                </address>
                                <!-- End .social-icons -->

                            </div><!-- End corporate-widget -->
                        </div><!-- End .widget -->
                    </div><!-- End .col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h4>Tentang Kami</h4>
                            <ul class="links">
                            <?php foreach ($profil as $d): ?>
                                <li><a href="<?=base_url() . '/' . $d->slug;?>"><?=$d->judul?></a></li>

                                        <?php endforeach;?>
                            </ul>
                        </div><!-- End .widget -->
                    </div><!-- End .col-md-3 -->

                    <div class="clearfix visible-sm"></div><!-- End .clearfix -->

                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h4>Area Download</h4>
                            <ul class="links">
      <?php foreach ($regulasi as $d): ?>
                                <li><a href="<?=base_url('regulasi') . '/' . $d->slug;?>"><?=$d->nama?></a></li>

                                        <?php endforeach;?>


                            </ul>
                        </div><!-- End .tagcloud -->
                    </div><!-- End .col-md-3 -->

                    <div class="col-md-3 col-sm-6">
                        <div class="widget">
                            <h4>Berita</h4>
                            <div class="widget">
                                <ul class="links">

                              <?php foreach ($berita as $d): ?>
                                <li><a href="<?=base_url('post/read') . '/' . $d->slug;?>"><?=$d->judul?></a></li>

                                        <?php endforeach;?>

                                    <li><a href="<?=base_url()?>/post/berita">Lihat semua Berita</a></li>
                                </ul>
                            </div><!-- End .newsletter-widget -->
                        </div><!-- End .widget -->
                    </div><!-- End .col-md-3 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End #footer-inner -->
        <div id="footer-bottom" style="background-color:#ffd83b; color:#000">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-push-6">
                    </div><!-- End .col-md-6 -->
                    <div class="col-md-6 col-md-pull-6">
                        <p class="copyright">Badan Kepegawaian, Pendidikan dan Pelatihan Kota Gorontalo. &copy; Tahun 2021</p>
                    </div><!-- End .col-md-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End #footer-bottom -->
    </footer><!-- End #footer -->

    </div><!-- End #wrapper -->
    <a href="#top" id="scroll-top" class="smaller no-radius" title="Back to Top"><i class="fa fa-angle-up"></i></a>
    <!-- END -->
