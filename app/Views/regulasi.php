<?=$this->extend('main');?>
<?=$this->section('content');?>
<div id="content" role="main">
    <div class="page-header dark larger larger-desc">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Kategori download: Peraturan</h1>
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-header -->

    <div class="container">
        <div class="row">

            <style type="text/css" media="screen">
                th,
                td {
                    /* text-align: left !important;	*/
                    vertical-align: top !important;
                    padding: 5px 12px !important;
                    color: #000 !important;
                }
            </style>

            <div class="col-md-12">
                <p class="alert alert-success">Berikut adalah berkas digital yang dapat Anda unduh dalam kategori
                    <strong>Peraturan</strong></p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover" id="dataTables-example"
                        style="width: 100%;">
                        <thead>
                            <tr>
                                </td><!-- melakukan penyesuaian judul kolom data unduhan-->
                                <th width="5%">No</th>
                                <th>Dokumen</th>
                                <th>Keterangan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
$no = 1;
foreach ($regulasi as $d) {?>

                            <tr>
                                <td style="text-align:center"><?=$no?></td>
                                <td style="width:45%"><?=$d->judul?>
                                </td><!-- melakukan penyesuaian lebar kolom -->
                                <td>
                                    <p><?=$d->deskripsi?></p>
                                </td>
                                <td>
                                    <a href="https://bkpp.gorontalokota.go.id/storage/regulasi/<?=$d->file?>"
                                        class="btn btn-primary btn-xs" target="_blank">
                                        <i class="fa fa-download"></i>&nbsp;Unduh</a>
                                </td>
                            </tr>


                        <?php $no++;}?>
                        </tbody>
                    </table>
                </div>
            </div><!-- End .row -->
        </div><!-- End .container -->

        <div class="mb20"></div><!-- space -->

    </div><!-- End #content -->
    <?=$this->endSection('content');?>