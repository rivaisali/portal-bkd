<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>
<div id="content" role="main">
    <div class="page-header dark larger larger-desc">
        <div class="container">
            <div class="row">
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-header -->

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <article class="entry single">
                    <span class="entry-date"><?php echo date('d', strtotime($post->tanggal)); ?><span><?php echo date('M', strtotime($post->tanggal)); ?></span></span>
                    <h1 class="entry-title"><?=$post->judul?></h1>
<div class="entry-media">
                                <a href="https://bkpp.gorontalokota.go.id/berita/kategori/seleksi-terbuka"
                                    class="category-block" title="<?=$post->kategori?>"><?=$post->kategori?></a>
                                <figure>
                                <a href="<?=base_url($post->slug)?>" title="Berita">
                                    <img src="https://bkpp.gorontalokota.go.id/storage/post/<?=$post->gambar?>"
                                        alt="Pemutakhiran Data Profil Pegawai">
                                </a>
                            </figure>
                            </div><!-- End .entry-media -->
                    <div class="entry-content">
                        <p style="text-align: justify;"><?=$post->isi?></p>
                    </div><!-- End .entry-content -->
                </article>
            </div>
            <!-- End 9 -->

            <div class="col-md-3">
                <div class="widget">
                    <div style="text-align:center">
                        <hr />
                        <h3><i class="fa fa-bookmark"></i>&nbsp;Berita Terkait</h3>
                        <hr />
                    </div>
                    <ul class="latest-posts-list" style="list-style-type:none; margin-left:-33px;">
                    <?php foreach ($lainnya as $d): ?>
                        <li class="clearfix">
                            <figure><img class="thumbnail"
                                    src="<?=base_url('assets/images/bgberitaputih.png')?>"
                                    alt="Rapat Kerja Teknis Kepegawaian, 15 Juni 2021"></figure>
                            <div class="entry-content">
                                <h5><a href="<?=base_url('post/read') . '/' . $d->slug?>">
                                        <?=$d->judul?></a></h5>
                                <p><?=$d->tanggal?> </p>
                            </div><!-- End .entry-content -->
                        </li>
                    <?php endforeach;?>

                    </ul>
                </div><!-- End .widget -->
            </div>


        </div><!-- End .row -->
    </div><!-- End .container -->
</div><!-- End #content -->
<div class="mb20"></div><!-- space -->
<?=$this->endSection('content');?>