 <?php
$model = new \App\Models\CustomModel;
$regulasi = $model->select_data("regulasi", "getResult");
$profil = $model->select_data("profil", "getResult");
$layanan = $model->select_data("layanan", "getResult", false, null, ["urutan", "ASC"]);
$tupoksi = $model->select_data("tupoksi", "getResult", false, null, ["urutan", "ASC"]);

?>
 <div class="collapse navbar-white" id="header-search-form">
         <div class="container">
                 <form action="<?=base_url('post/filter')?>" class="navbar-form animated fadeInDown" method="POST" accept-charset="utf-8">
                         <?=csrf_field()?>
                         <input type="search" id="s" name="query" class="form-control" placeholder="Pencarian...">
                         <button type="submit" title="Search"><i class="fa fa-search"></i></button>

                 </form>
         </div><!-- End .container -->
 </div><!-- End #header-search-form -->
 <nav class="navbar navbar-white animated-dropdown btt-dropdown" role="navigation">
         <!-- header menu paling atas -->
         <div class="navbar-top navbar-color clearfix" style="background-color:#ffd83b">
                 <div class="container">
                         <div class="pull-left">
                                 <ul class="navbar-top-nav clearfix hidden-sm hidden-xs">
                                         <li><a style="color:black;" href="<?=base_url()?>/kontak"><i
                                                                 class="fa fa-phone"></i>0435821073</a>
                                         </li>
                                         <li><a href="<?=base_url()?>/kontak"><i class="fa fa-fax"></i>(0435) 821073</a>
                                         </li>
                                         <li><a href="<?=base_url()?>/kontak"><i
                                                                 class="fa fa-envelope"></i>bkpp@gorontalokota.go.id</a>
                                         </li>
                                 </ul>
                         </div><!-- End .pull-left -->

                         <div class="pull-right">
                                 <div class="social-icons pull-right hidden-xs">

                                 </div><!-- End .social-icons -->

                                 <div class="dropdowns-container pull-right clearfix">
                                         <div class="dropdown currency-dropdown pull-right">
                                                 <ul class="navbar-top-nav clearfix hidden-sm hidden-xs">
                                                         <li><a href="<?=base_url()?>/kontak"><i
                                                                                 class="fa fa-building"></i>Hubungi
                                                                         Kami</a></li>
                                                 </ul>

                                         </div><!-- End .currency-dropdown -->


                                 </div><!-- End. dropdowns-container -->

                         </div><!-- End .pull-right -->
                 </div><!-- End .container -->
         </div><!-- End .navbar-top -->
         <style>
                 .dropdown hr {
                         margin: 4px 0 !important;
                 }
         </style>


         <div class="navbar-inner sticky-menu">
                 <div class="container">
                         <div class="navbar-header">

                                 <button type="button" class="navbar-toggle pull-right collapsed" data-toggle="collapse"
                                         data-target="#main-navbar-container">
                                         <span class="sr-only">Toggle navigation</span>
                                         <span class="icon-bar"></span>
                                 </button>

                                 <a class="navbar-brand clear-padding-left text-uppercase" href="<?=base_url()?>"
                                         title="Badan Kepegawaian,Pendidikan dan Pelatihan Kota Gorontalo"><img
                                                 src="<?=base_url()?>/assets/images/logo.png"
                                                 style="max-height: 50px; width: auto;"></a>

                                 <button type="button" class="navbar-btn btn-icon pull-right last visible-sm visible-xs"
                                         data-toggle="collapse" data-target="#header-search-form"><i
                                                 class="fa fa-search"></i></button>



                         </div><!-- End .navbar-header -->

                         <div class="collapse navbar-collapse" id="main-navbar-container">
                                 <ul class="nav navbar-nav">
                                         <li><a style="color:#e67e22" href="<?=base_url()?>"><i class="fa fa-home"
                                                                 aria-hidden="true"></i>&nbsp;Beranda</a></li>

                                         <li class="dropdown">
                                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                         role="button" aria-expanded="false">
                                                         <i class="fa fa-newspaper-o"
                                                                 aria-hidden="true"></i>&nbsp;Berita<span
                                                                 class="angle"></span></a>
                                                 <ul class="dropdown-menu" role="menu">

                                                         <li><a href="<?=base_url('post/berita')?>">Berita</a>
                                                         </li>
                                                         <li><a href="<?=base_url('post/pengumuman')?>">Pengumuman</a>
                                                         </li>
                                                         <li><a href="<?=base_url('post/agenda')?>">Agenda</a>
                                                         </li>

                                                 </ul>
                                         </li>

                                         <!-- Menu Dinamis dari database -->



                                         <!-- Bidang -->
                                         <li class="dropdown">
                                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                         role="button" aria-expanded="false">
                                                         <i class="fa fa-university"
                                                                 aria-hidden="true"></i>&nbsp;Profil<span
                                                                 class="angle"></span></a>
                                                 <ul class="dropdown-menu" role="menu">
                                                         <?php foreach ($profil as $d): ?>
                                                         <li><a href="<?=base_url('profil') . '/' . $d->slug?>"><?=$d->judul?></a>
                                                         </li>

                                                         <?php endforeach;?>

                                                 </ul>

                                                 </li>
                                                      <li><a  href="<?=base_url('profil/pegawai')?>"><i class="fa fa-user"></i>&nbsp;Profil Pegawai</a></li>

                                                 <!-- Statistik -->
                                         <li class="dropdown">
                                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                         role="button" aria-expanded="false">
                                                         <i class="fa fa-bar-chart"
                                                                 aria-hidden="true"></i>&nbsp;Statistik<span
                                                                 class="angle"></span></a>
                                                 <ul class="dropdown-menu" role="menu">
                                                         <li><a href="<?=base_url()?>/statistik/jenis-kelamin">Jenis
                                                                         Kelamin</a></li>
                                                         <li><a href="<?=base_url()?>/statistik/usia">Usia</a></li>
                                                         <li><a href="<?=base_url()?>/statistik/eselon">Eselon</a></li>
                                                         <li><a href="<?=base_url()?>/statistik/jabatan-camat-lurah">Jabatan
                                                                         Camat Lurah</a>
                                                         </li>
                                                         <li><a href="<?=base_url()?>/statistik/status-pegawai">Status
                                                                         Pegawai</a></li>
                                                         <li><a href="<?=base_url()?>/statistik/jabatan">Jabatan</a>
                                                         </li>
                                                 </ul>
                                         </li>

                                         <!-- Layanan -->
                                         <li class="dropdown">
                                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                         role="button" aria-expanded="false">
                                                         <i class="fa fa-server"
                                                                 aria-hidden="true"></i>&nbsp;Pelayanan<span
                                                                 class="angle"></span></a>
                                                 <ul class="dropdown-menu" role="menu">

                                                         <?php foreach ($layanan as $d): ?>
                                                         <li><a href="<?=base_url('pelayanan') . '/' . $d->slug?>"><?=$d->judul?></a>
                                                         </li>

                                                         <?php endforeach;?>


                                                 </ul>
                                         </li>

                                         <li class="dropdown megamenu-container">
                                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                         role="button" aria-expanded="false">
                                                         <i class="fa fa-list" aria-hidden="true"></i>&nbsp;Tupoksi<span
                                                                 class="angle"></span></a>
                                                 <div class="dropdown-menu megamenu" role="menu">
                                                         <div class="container">
                                                                 <div class="row">
                                                                         <?php foreach ($tupoksi as $d): ?>
                                                                         <div class="col-md-3">
                                                                                 <a href="<?=base_url("tupoksi") . '/' . $d->slug?>"
                                                                                         class="megamenu-title"><?=$d->judul?></a>
                                                                                 <?php
$subtupoksi = $model->select_data("tupoksi_sub", "getResult", ["tupoksi_id" => $d->id], null, ["urutan", "ASC"]);
if ($subtupoksi) {
    echo "<ul>";
    foreach ($subtupoksi as $s): ?>

                                         <li><a href="<?=base_url("sub-tupoksi") . '/' . $s->slug?>"><?=$s->judul?></a>
                                         </li>
                                         <?php endforeach;?>

                                 </ul>
                                 <?php }?>
                         </div>
                         <?php endforeach;?>

                         <div class="clearfix"></div>


                 </div><!-- End .row -->
         </div><!-- End .container -->
         </div><!-- End .megamenu -->
         </li>

         <!-- Download -->
         <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                         <i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp;UNDUH<span
                                 class="angle"></span></a>
                 <ul class="dropdown-menu" role="menu">
                         <?php foreach ($regulasi as $d): ?>
                         <li><a href="<?=base_url('regulasi') . '/' . $d->slug?>"><?=$d->nama?></a>
                         </li>

                         <?php endforeach;?>
                 </ul>
         </li>


         <li>
                 <button type="button" class="navbar-btn btn-icon navbar-right last  hidden-sm hidden-xs"
                         data-toggle="collapse" data-target="#header-search-form"><i class="fa fa-search"></i></button>
         </li>
         </ul>


         </div><!-- /.navbar-collapse -->
         </div><!-- /.container -->
         </div><!-- End .navbar-inner -->
 </nav>