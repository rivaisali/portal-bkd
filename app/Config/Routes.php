<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Main');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Main::index');

$routes->get('post/berita', 'Main::post/berita');
$routes->get('post/read/(:any)', 'Main::post_detail/$1');
$routes->post('post/filter', 'Main::filter');
$routes->get('post/result/(:any)', 'Main::result/$1');

$routes->get('post/pengumuman', 'Main::post/pengumuman');
$routes->get('post/agenda', 'Main::post/agenda');

$routes->get('profil/pegawai', 'Main::profilPegawai');
$routes->post('profil/pegawai', 'Main::profilPegawai');
$routes->get('profil/pegawai/(:any)', 'Main::detailPegawai/$1');

$routes->get('profil/(:any)', 'Main::profil/$1');
$routes->get('pelayanan/(:any)', 'Main::pelayanan/$1');
$routes->get('regulasi/(:any)', 'Main::regulasi/$1');
$routes->get('tupoksi/(:any)', 'Main::tupoksi/$1');
$routes->get('sub-tupoksi/(:any)', 'Main::subtupoksi/$1');

$routes->get('statistik/jenis-kelamin', 'Main::statistik/jenis_kelamin');
$routes->get('statistik/usia', 'Main::statistik/usia');
$routes->get('statistik/status-pegawai', 'Main::statistik/status');
$routes->get('statistik/eselon', 'Main::statistik/eselon');
$routes->get('statistik/jabatan', 'Main::statistik/jabatan');
$routes->get('statistik/jabatan-camat-lurah', 'Main::statistik/jabcamatlurah');

$routes->get('kontak', 'Main::kontak');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
