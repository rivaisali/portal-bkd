<?=$this->extend('main');?>
<?=$this->section('content');?>
<style>
    .bg-slider {
        background-color: #009688;
        opacity: 0.8;
        padding: 20px;
        border-radius: 10px;
    }

    .bg-slider h2 {
        color: #FFF !important;
    }

    .bg-slider p {
        font-size: 14px !important;
    }

    #revslider ul li img,
    #revslider-container {
        max-height: 500px !important;
    }
</style>
   <div class="newsletter-popup mfp-hide clearfix" id="newsletter-popup-form">
            <div class="row">
                <div class="col-md-12" style="width: auto;">

                    <img src="https://bkpp.gorontalokota.go.id/storage/post/<?=$popup->gambar?>" alt="Nilai Budaya Kerja dan Perilaku Utama" class="img img-responsive"
                    style="width:auto; margin-bottom:10px">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="color:#000">
                    <h2 class="title-underblock custom mb25"><?=$popup->judul?></h2>
                    <p style="color: black;"> <?=$popup->isi?></p>
                    <p><a href="<?=base_url("post/read/" . $popup->slug)?>" class="btn btn-success btn-xs">
                        <i class="fa fa-forward"></i> Baca Selengkapnya</a></p>
                </div>
            </div>
        </div>

<section class="section home-section" id="home">
    <div id="revslider-container" class="container">
 <div class="row">
        <div id="revslider">
            <ul>
            <?php foreach ($terbaru as $d) {?>
                <li data-transition="fade" data-slotamount="33" data-masterspeed="800"
                    data-title="<?=$d->judul?>">
                    <img class="img-responsive" src="https://bkpp.gorontalokota.go.id/storage/post/wide_<?=str_replace(" ", "%20", $d->gambar)?>" alt="slidebg1">
                </li>
            <?php }?>



            </ul>
        </div><!-- End #revslider -->
    </div><!-- End #revslider-container -->

</section>
<div id="content" role="main" style="background-image: url(<?=base_url('assets/images/crossword.png')?>);">


    <div class="mb40"></div><!-- space -->

    <div class="container">

        <div class="col-md-12">
            <div class="post-section">
                <h2 class="title-border title-bg-line blue mb30"><span>Tajuk Berita</span></h2>

                <div class="owl-carousel mpopular-posts smaller-nav no-radius">


                    <?php
foreach ($post as $d): ?>
                    <article class="entry entry-overlay entry-block ">
                        <a href="https://bkpp.gorontalokota.go.id/berita/kategori/berita" class="category-block"
                            title="Berita"><?=$d->kategori?></a>
                        <div class="entry-media">
                            <figure>
                                <a href="<?=base_url($d->slug)?>" title="Berita">
                                    <img src="https://bkpp.gorontalokota.go.id/storage/post/<?=$d->gambar?>"
                                        alt="Pemutakhiran Data Profil Pegawai">
                                </a>
                            </figure>
                            <div class="entry-overlay-meta">
                                <span class="entry-overlay-date"><i class="fa fa-calendar"></i><?=$d->tanggal?></span>
                                <span class="entry-separator">/</span>
                                <a href="#" class="entry-comments"><i class="fa fa-eye"></i>51003</a>
                                <span class="entry-separator">/</span>
                                <a href="#" class="entry-author"><i class="fa fa-user"></i>Administrator</a>
                            </div><!-- End .entry-overlay-meta -->
                        </div><!-- End .entry-media -->

                        <h3 class="entry-title"><a href="<?=base_url("post/read/" . $d->slug)?>"><?=$d->judul?></a></h3>
                    </article>
                    <?php endforeach;?>

                </div><!-- End .mpopular-posts -->
            </div><!-- End .post-section -->

            <div class="post-section">
                <h2 class="title-border title-bg-line blue mb30"><span>Berita Terbaru</span></h2>

                <div class="row">
                    <div class="col-sm-12">
                        <?php foreach ($terbaru as $d): ?>
                        <article class="entry entry-overlay entry-block eb-small green col-md-6">
                            <div class="entry-media">
                                <a href="https://bkpp.gorontalokota.go.id/berita/kategori/seleksi-terbuka"
                                    class="category-block" title="<?=$d->kategori?>"><?=$d->kategori?></a>
                                <figure class="img-thumbnail" style="width:110px; height:110px;">
                                    <a href="https://bkpp.gorontalokota.go.id/berita/read/" title="<?=$d->judul?>">
                                        <img src="<?=base_url('assets/images/bgberitaputih.png')?>"
                                            alt="<?=$d->judul?>">
                                    </a>
                                </figure>
                            </div><!-- End .entry-media -->
                            <h3 style="padding-right:10px;" class="entry-title"><a
                                    href="https://bkpp.gorontalokota.go.id/berita/read/"><?=$d->judul?></a></h3>
                            <div class="entry-meta">
                                <span class="entry-overlay-date"><i class="fa fa-calendar"></i>26 Jun 21</span>
                                <span class="entry-separator">/</span>
                                <a href="#" class="entry-eye">481</a>
                                <span class="entry-separator">/</span>
                                Oleh <a href="#" class="entry-author">Administrator</a>
                            </div><!-- End .entry-meta -->
                        </article>
                        <?php endforeach;?>
                    </div><!-- End .col-sm-12 -->
                </div><!-- End .row -->
            </div><!-- End .post-section -->

<div class="text-center">
    <a href="<?=base_url('post/berita')?>" class="btn btn-dark btn-border no-radius min-width-md"><strong>Berita lainnya</strong></a>
    </div>
        </div><!-- End .blog-item-container -->
    </div><!-- End .container-fluid -->
</div>

</div><!-- End #content -->
<div class="mb50 mb10-xs"></div><!-- space -->
<div style="background-image: url(<?=base_url('assets/images/crossword.png')?>);">
    <div class="mb50 mb10-xs"></div><!-- space -->
    <div class="container">

        <div class="post-section">
            <h2 class="title-border title-bg-line blue mb30">
                <span>Aplikasi & Instansi Terkait</span>
            </h2>

            <div class="owl-carousel mpopular-posts smaller-nav no-radius">

                <?php foreach ($aplikasi as $d): ?>
                <article class="entry entry-overlay entry-block text-center">
                    <a href="<?=$d->link?>" class="category-block" title="Simpeg"><?=$d->nama_aplikasi?></a>
                    <div class="entry-media">
                        <figure>
                            <a href="<?=$d->link?>" title="<?=$d->nama_aplikasi?>">
                                <img src="https://bkpp.gorontalokota.go.id/storage/icon/<?=$d->icon?>" alt="Simpeg">
                            </a>
                        </figure>
                    </div><!-- End .entry-media -->

                    <h3 class="entry-title"><a href="<?=$d->link?>"><?=$d->deskripsi?></a></h3>
                </article>
                <?php endforeach;?>
            </div>
        </div>
    </div><!-- End .mpopular-posts -->
</div><!-- End .post-section -->
<script src="assets/charts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="assets/charts/amcharts/serial.js" type="text/javascript"></script>


<div class="pt60 pb10-xs" style="background-image:url(<?=base_url('assets/images/graphy.png');?>)">
    <div class="container">
        <h2 class="title-underblock light text-center mb50">Rekapitulasi Jumlah Pegawai Negeri Sipil Berdasarkan Status Pemerintah
            Kota Gorontalo Tahun <?=date('Y')?></h2>
        <div class="row">


            <script>
                var chart;

                var chartData = [
                    <?php foreach ($status as $d): ?>
    {
        "data": "<?php echo $d->nmstatuspns ?>",
        "value": <?php echo status($d->statuspns) ?>
    },
   <?php endforeach;?>
                ];


                AmCharts.ready(function () {
                    // SERIAL CHART
                    chart = new AmCharts.AmSerialChart();
                    chart.dataProvider = chartData;
                    chart.categoryField = "data";
                    chart.startDuration = 1;

                    // AXES
                    // category
                    var categoryAxis = chart.categoryAxis;
                    categoryAxis.labelRotation = 0;
                    categoryAxis.gridPosition = "start";
                    categoryAxis.gridThickness = 0;
                     categoryAxis.labelRotation = 45;

                    var valueAxis = new AmCharts.ValueAxis();
                    valueAxis.gridThickness = 0;
                    chart.addValueAxis(valueAxis);

                    // GRAPH
                    var graph = new AmCharts.AmGraph();
                    graph.valueField = "value";
                    graph.balloonText = "[[category]]: <b>[[value]]</b>";
                    graph.labelText = "[[value]]";
                    graph.type = "column";
                    graph.lineAlpha = 0;
                    graph.fillAlphas = 0.8;
                    chart.addGraph(graph);

                    // CURSOR
                    var chartCursor = new AmCharts.ChartCursor();
                    chartCursor.cursorAlpha = 0;
                    chartCursor.zoomable = false;
                    chartCursor.categoryBalloonEnabled = false;
                    chart.addChartCursor(chartCursor);

                    chart.creditsPosition = "top-right";

                    // WRITE
                    chart.write("chartdiv218");
                });
            </script>
            <div class="link_grafik" id="chartdiv218" style="width: 100%; min-height: 300px;"></div>
            <div class="mb50"></div>

        </div><!-- End .row -->
    </div><!-- End .container -->
</div>
<br>
<h2 class="title-underblock dark text-center mb50">Audio dan Video</h2>
<div class="bg-image">

    <div class="container">
        <div class="col-md-6">
            <div class="iframe-btn-container">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"
                        src="https://www.youtube-nocookie.com/embed/HaCIQhAA6U8"></iframe>
                </div>
            </div><!-- End .iframe-btn-container -->
        </div>
        <div class="col-md-6">
            <div class="iframe-btn-container">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"
                        src="https://www.youtube-nocookie.com/embed/IzQHWryMQvs"></iframe>
                </div>
            </div><!-- End .iframe-btn-container -->
        </div>

    </div><!-- End .container -->
    <div class="mb50"></div>
    <!-- Penambahan Lagu Indonesia Raya dalam rangka HUT RI Ke-73 Tahun 2018 #BKD13 -->
    <div class="container" style="text-align:center;">
        <!-- <audio controls autoplay allow="autoplay" loop style="width:100%">		-->
        <div class="panel panel-success">
            <div class="panel-heading" style="padding:10px;">
                <h3 class="panel-title">Lagu Kebangsaan Republik Indonesia - Indonesia Raya</h3>
            </div>
            <div class="panel-body">
                <audio controls style="width:100%">
                    <!-- menonaktifkan autoplay -->
                    <source src="https://upload.wikimedia.org/wikipedia/commons/3/32/Indonesiaraya.ogg"
                        type="audio/ogg">
                    Browser Anda tidak mendukung untuk memutar elemen audio.
                </audio>
            </div>
        </div>
    </div>
    <div class="mb50"></div>
</div>
<div class="mb50"></div>

<?php
function status($id)
{

    $model = new \App\Models\SimpegModel;
    $status = $model->select_data_count("r_dip", "getRow", ["statuspns" => $id]);
    return $status->count;

}
?>
<?=$this->endSection('content');?>