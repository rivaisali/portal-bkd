<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>
<div id="content" role="main">
    <div class="page-header dark larger larger-desc">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Kategori : Berita</h1>
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <div class="mb40"></div><!-- space -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <?php foreach ($post as $d): ?>
                    <div class="col-md-6">
                        <article class="entry">
                            <span class="entry-date"><?php echo date('d', strtotime($d->tanggal)); ?><span><?php echo date('M', strtotime($d->tanggal)); ?>
                                </span></span>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 class="entry-title"><a
                                            href="<?=base_url('post/read') . '/' . $d->slug?>"><?=substr($d->judul, 0, 50)?>...</a></h2>
                                    <div class="entry-media">
                                        <a href="<?=base_url('post') . '/' . $d->kategori?>"
                                            class="category-block" title="<?=$d->kategori?>"><?=$d->kategori?></a>
                                        <figure>
                                            <a href="<?=base_url('post/read') . '/' . $d->slug?>" title="Berita">
                                                <img src="https://bkpp.gorontalokota.go.id/storage/post/<?=$d->gambar?>"
                                                    alt="<?=$d->judul?>">
                                            </a>
                                        </figure>
                                    </div><!-- End .entry-media -->
                                    <div class="entry-content">
                                        <p><?=substr(strip_tags($d->isi), 0, 250)?> ...</p>
                                    </div><!-- End .entry-content -->
                                    <footer class="entry-footer clearfix">
                                        <span class="entry-cats">
                                            <span class="entry-label"><i class="fa fa-tag"></i>Kategori:</span><a
                                                href="<?=base_url('post') . '/' . $d->kategori?>"><?=$d->kategori?></a>
                                        </span><!-- End .entry-tags -->
                                        <span class="entry-separator">/</span>
                                        Oleh <a href="#" class="entry-author">Administrator</a>
                                    </footer>
                                </div><!-- End .col-md-6 -->
                            </div><!-- End .row -->

                        </article>
                    </div>

                    <?php endforeach;?>

        <div class="clearfix"></div>

                </div>
            </div>


        </div>


    </div><!-- End .row -->
</div>
<?=$this->endSection('content');?>