<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>
<script src="<?=base_url('assets/charts/amcharts/plugins/export/export.min.js');?>"></script>
<link rel="stylesheet" href='<?=base_url('assets/charts/amcharts/plugins/export/export.css')?>' type='text/css' media='all'/>
<script src="<?=base_url('assets/charts/amcharts/amcharts.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/charts/amcharts/pie.js')?>" type="text/javascript"></script>


<div id="content" role="main">
<br><br>

<div class="container">
<div class="row">

    <script>
            var chart;
            var legend;

            var chartData208 = [
                                {
                    "data208": "Kurang dari 25",
                    "value208": <?=$usia25[0]->count?> },
                              {
                    "data208": "25 s.d. 30",
                    "value208": <?=$usia25sampai30[0]->count?>                },
                              {
                    "data208": "31 s.d. 36",
                    "value208": <?=$usia31sampai36[0]->count?>                 },
                              {
                    "data208": "37 s.d. 42",
                    "value208": <?=$usia37sampai42[0]->count?>                 },
                              {
                    "data208": "43 s.d. 48",
                    "value208": <?=$usia43sampai48[0]->count?>                 },
                              {
                    "data208": "49 s.d. 55",
                    "value208": <?=$usia49sampai55[0]->count?>                 },
                              {
                    "data208": "Lebih dari 55",
                    "value208": <?=$usia55[0]->count?>                 },
                          ];

            AmCharts.ready(function () {
                // PIE CHART
                chart = new AmCharts.AmPieChart();
                chart.dataProvider = chartData208;
                chart.titleField = "data208";
                chart.valueField = "value208";
                chart.outlineColor = "#FFFFFF";
                chart.outlineAlpha = 0.8;
                chart.outlineThickness = 2;
                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                // this makes the chart 3D
                chart.depth3D = 15;
                chart.angle = 30;
                // WRITE
                chart.write("chartdiv207");

            });


        </script>


            <div class="panel panel-success">
                <div class="panel-heading" style="padding:15px 0px 0px 15px">
                    <h2>Rekapitulasi Jumlah Pegawai Berdasarkan Usia</h2>
                </div>

                <div class="panel-body isi">



                                    <div class="col-md-5">


                        <p>Rekapitulasi Jumlah Pegawai Negeri Sipil (PNS & CPNS) di Lingkungan Pemerintah Kota Gorontalo Berdasarkan Jenis Kelamin</p>

                       <table class="table table-bordered table-hover table-striped" width="100%">
                        <thead class="bordered-darkorange">
                        <tr>
                            <th style="text-align:center; width:40px;">No</th>
                            <th class="text-center">Keterangan</th>
                            <th class="text-center">Jumlah</th>
                        </tr>
                        </thead>
                       <tbody>
                           <tr class="odd gradeX">
                               <td class="text-center">1</td>
                               <td>Kurang dari 25</td>
                               <td><?=$usia25[0]->count?></td>
                           </tr>
                           <tr class="odd gradeX">
                               <td class="text-center">2</td>
                               <td>25 s.d. 30</td>
                               <td><?=$usia25sampai30[0]->count?>    </td>
                           </tr>
                           <tr class="odd gradeX">
                               <td class="text-center">3</td>
                               <td>31 s.d. 36</td>
                               <td><?=$usia31sampai36[0]->count?>    </td>
                           </tr>
                           <tr class="odd gradeX">
                               <td class="text-center">4</td>
                               <td>37 s.d. 42</td>
                               <td><?=$usia37sampai42[0]->count?>    </td>
                           </tr>
                           <tr class="odd gradeX">
                               <td class="text-center">5</td>
                               <td>43 s.d. 48</td>
                               <td><?=$usia43sampai48[0]->count?>    </td>
                           </tr>
                           <tr class="odd gradeX">
                               <td class="text-center">6</td>
                               <td>49 s.d. 55</td>
                               <td><?=$usia49sampai55[0]->count?>    </td>
                           </tr>
                           <tr class="odd gradeX">
                               <td class="text-center">7</td>
                               <td>Lebih dari 55</td>
                               <td><?=$usia55[0]->count?>    </td>
                           </tr>
                           <tr style="font-weight:bold">
                               <td colspan="2" class="text-center">JUMLAH</td>
                               <td><?=($usia25[0]->count) + ($usia25sampai30[0]->count) + ($usia31sampai36[0]->count) + ($usia37sampai42[0]->count) + ($usia43sampai48[0]->count) + ($usia49sampai55[0]->count) + ($usia55[0]->count)?></td>
                           </tr>
                       </tbody>
                        </table>

                    </div>

                    <div class="col-md-7">

                        <div class="link_grafik" id="chartdiv207" style="width: 100%; height: 400px;"></div>
                                            </div>


                </div>

<div class="panel-footer isi">
	<div class="row">
		<div class="col-md-7 text-left">
			<table>
				<tr>
					<td style="width:50px"><i class="fa fa-3x fa-info-circle" style=" vertical-align: middle;color:#7f8c8d"></i></td>
					<td><div style="color:#7f8c8d">Data ini bersifat statis dan dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya.</div></td>
				</tr>
			</table>
		</div>
	</div>
</div>
</div>
</div>



</div>


 </div><!-- End .row -->
</div><!-- End .container -->
<?=$this->endSection('content');?>