<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>
<div id="content" role="main">
    <div class="page-header dark larger larger-desc">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?=$pelayanan->judul?></h1>
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-header -->

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div style="color:#333;">
                <div id="pdf_container" style="background-image: url('https://bkpp.gorontalokota.go.id/img/loading.gif'); background-repeat: no-repeat; background-position: 50% 30%; height: 720px; width: 100%;">
                            Loading file PDF...
                            </div>
                 <script type="text/javascript">
                            <?php $pdf = $pelayanan->pdf;?>
                                document.getElementById("pdf_container").innerHTML = '<object type="application/pdf" data="https://bkpp.gorontalokota.go.id/storage/layanan/pdf/<?php echo $pdf; ?>" width="100%" height="720px"><p>This web browser does not support PDF viewer</p></object>';
                            </script>
                <?php if ($pelayanan->gambar != null) {?>
                <img class="img-responsive" src="https://bkpp.gorontalokota.go.id/storage/profil_tupoksi/<?=$profil->gambar?>">
                <?php }?>
                <p><?=$pelayanan->isi?></p>
            </div>
             </div>
        </div>
    </div>
</div>
<?=$this->endSection('content');?>