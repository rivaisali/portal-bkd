<!DOCTYPE html>
<html>
<head>
  <?=$this->include("_partials/head.php")?>
</head>

<body data-spy="scroll" data-target="#main-navbar-container">
    <div class="boss-loader-overlay"></div>
    <!-- End .boss-loader-overlay -->
    <div id="wrapper" class="bg-white kotak">
        <header id="header" role="banner" style="border-top-color: #FFB900">

    <!-- Navbar -->
    <?=$this->include("_partials/navbar.php")?>
    <!-- /.navbar -->
     </header>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <section class="content">
        <?=$this->renderSection('content')?>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?=$this->include("_partials/footer.php")?>

  <?=$this->include("_partials/js.php")?>
</body>

</html>