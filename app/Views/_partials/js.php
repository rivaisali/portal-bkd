 <!-- Google map javascript api v3 -->

    <!-- Smoothscroll -->
    <script src="<?php echo base_url('assets/main/js/smoothscroll.js') ?>"></script>

    <script src="<?php echo base_url('assets/main/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/jquery.hoverIntent.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/jquery.nicescroll.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/waypoints.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/waypoints-sticky.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/jquery.debouncedresize.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/jquery.themepunch.tools.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/jquery.themepunch.revolution.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/owl.carousel.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/isotope.pkgd.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/skrollr.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/jquery.magnific-popup.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/jquery.countTo.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/jquery.validate.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/contact.js') ?>"></script>
    <script src="<?php echo base_url('assets/main/js/main.js') ?>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>


    <script>
        (function () {
            "use strict";


            // Slider Revolution
            jQuery('#revslider').revolution({
                delay: 8000,
                startwidth: 1200,
                startheight: 500,
                fullWidth: "on",
                fullScreen: "on",
                hideTimerBar: "on",
                spinner: "spinner4",
                fullScreenOffsetContainer: "#header",
                navigationStyle: "preview4",
                soloArrowLeftHOffset: 20,
                soloArrowRightHOffset: 20,
            });


        }());
    </script>

    <script src="<?php echo base_url('assets/datatable/media/js/jquery.dataTables.js') ?>"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                language: {
                    paginate: {
                        previous: '< Sebelumnya',
                        next: 'Selanjutnya >'
                    },
                    aria: {
                        paginate: {
                            previous: 'Sebelumnya',
                            next: 'Selanjutnya'
                        }
                    }
                }
            });
        });
    </script>
