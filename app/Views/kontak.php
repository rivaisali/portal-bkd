<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>
<div id="content" role="main">
<div class="page-header dark larger larger-desc">
<div class="container">
<div class="row">
    <div class="col-md-12">
        <h1>Kontak Badan Kepegawaian, Pendidikan dan Pelatihan Kota Gorontalo</h1>
    </div><!-- End .col-md-6 -->
</div><!-- End .row -->
</div><!-- End .container -->
</div><!-- End .page-header -->

<div class="container">
<div class="row kontak">
  <div class="col-sm-6 col-xs-12  alert alert-success">
    <div class="media addressContent">
      <span class="media-left bg-color-1" href="#">
        <i class="fa fa-map-marker" aria-hidden="true"></i>
      </span>
      <div class="media-body">
        <h3 class="media-heading">Alamat:</h3>
<?=$pengaturan[0]->alamat;?></p>
      </div>
    </div>

    <div class="media addressContent">
      <span class="media-left bg-color-2" href="#">
        <i class="fa fa-phone" aria-hidden="true"></i>
      </span>
      <div class="media-body">
        <h3 class="media-heading">Telepon/HP:</h3>
        <p><i class="fa fa-phone"></i> <?=$pengaturan[0]->telepon;?>       <br><i class="fa fa-mobile"></i> -        </p>
      </div>
    </div>

    <div class="media addressContent">
      <span class="media-left bg-color-3" href="#">
        <i class="fa fa-envelope-o" aria-hidden="true"></i>
      </span>
      <div class="media-body">
        <h3 class="media-heading">Email:</h3>
        <p><a href="mailto:<?=$pengaturan[0]->email;?>"><i class="fa fa-envelope"></i> <?=$pengaturan[0]->email;?></a>
          <br>
          <a href="mailto:<?=$pengaturan[0]->email;?>"><i class="fa fa-envelope"></i> <?=$pengaturan[0]->email;?></a>
        </p>
      </div>
    </div>



  </div>
  <div class="col-sm-6 col-xs-12">
    <div class="homeContactContent">
      <h3 class="media-heading" style="text-transform: capitalize !important;">Mengubungi kami</h3>
      <p>Anda dapat menghubungi kami melalui formulir di bawah ini</p>

            <form action="<?=base_url()?>/kontak" method="post" role="form">
        <div class="row">
          <div class="col-sm-6 col-xs-12">
            <div class="form-group">
             <label for="nama">Nama Anda</label>
              <input type="text" name="nama" class="form-control border-color-1" id="exampleInputEmail1" placeholder="Nama lengkap" required="required" value="">
            </div>
          </div>
          <div class="col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="email">Email Anda</label>
              <input type="text" name="email" class="form-control border-color-2" id="exampleInputEmail2" placeholder="Email"  required="required" value="">
            </div>
          </div>
          <div class="col-sm-6 col-xs-12">
            <div class="form-group">
             <label for="pesan">Telepon/HP Anda</label>
              <input type="text" name="telepon" class="form-control border-color-5" id="exampleInputEmail3" placeholder="Telepon/HP"  required="required" value="">
            </div>
          </div>
          <div class="col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="judul">Judul Anda</label>
              <input type="text" name="judul" class="form-control border-color-6" id="exampleInputEmail4" placeholder="Judul pesan"  required="required" value="">
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <label for="pesan">Pesan Anda</label>
              <textarea name="pesan" class="form-control border-color-4" placeholder="Tulis Pesan"  required="required"></textarea>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="formBtnArea">
              <button type="submit" class="btn btn-primary">Kirim Pesan</button>
              <button type="reset" class="btn btn-primary">Reset</button>
            </div>
          </div>
        </div>
      </form>
</div>
</div></div>
<div class="row">
  <div class="col-xs-12">
    <div class="mapArea areaPadding"><br>
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1875.3413845058185!2d123.058977!3d0.532611!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1bba6b53742766e4!2sBADAN%20KEPEGAWAIAN%20PENDIDIKAN%20DAN%20PELATIHAN%20KOTA%20GORONTALO!5e1!3m2!1sid!2sus!4v1624930559776!5m2!1sid!2sus" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>      </div>


    </div>
  </div>
</div>


</div><!-- End .row -->
</div><!-- End .container -->
</div><!-- End #content -->
<?=$this->endSection('content');?>