<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>

<div id="content" role="main">
    <div class="page-header dark larger larger-desc">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Cari Profil ASN</h1>
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <div class="container">
        <div class="row">

            <div class="panel panel-success">
                <div class="panel-heading" style="padding:15px 0px 0px 15px">
            <h3>Silahkan Masukan Nama Pegawai</h3>
                </div>
                <div class="panel-body">

                    <form action="<?=base_url('profil/pegawai')?>" method="POST">
                        <?=csrf_field()?>
                        <div class="row">
                            <div class="form-group col-md-10">

                                <input type="text" name="key" class="form-control" placeholder="Masukan Nama Pegawai">
                            </div>
                            <div class="group col-md-2">

                                <button class="btn btn-primary" type="submit" name="filterpegawai" title="Search"><i
                                        class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>

            <?php
if (isset($_POST['filterpegawai'])) {?>
            <div class="panel panel-success">
                <div class="panel-heading" style="padding:15px 0px 0px 15px">
                    <h3>Data Pegawai</h3>
                </div>

                <div class="panel-body isi">


                    <div class="col-md-12">




                        <table class="table table-bordered table-hover table-striped" width="100%">
                            <thead class="bordered-darkorange">
                                <tr>
                                    <th style="text-align:center; width:40px;">No</th>
                                    <th class="text-center">Nama Lengkap</th>
                                    <th class="text-center">Jabatan</th>
                                     <th class="text-center">Unit Organisasi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;foreach ($data as $d): ?>
                                <tr>
                                    <td class="text-center"><?=$no?></td>
                                    <td><a href="<?=base_url('profil/pegawai') . '/' . $d->nip2?>"><?=$d->nama?></a></td>
                                    <td><?=$d->jabatann?></td>
                                     <td><?=$d->unorname?></td>
                                </tr>
                                <?php $no++;endforeach;?>
                            </tbody>
                        </table>

                    </div>



                </div>

                <div class="panel-footer isi">
                    <div class="row">
                        <div class="col-md-7 text-left">
                            <table>
                                <tr>
                                    <td style="width:50px"><i class="fa fa-3x fa-info-circle"
                                            style=" vertical-align: middle;color:#7f8c8d"></i></td>
                                    <td>
                                        Catatan : Jika data anda tidak sesuai, harap hubungi SKPD atau Biro Kepegawaian di instansi saudara dengan membawa dokumen yang otentik.
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <?php }?>



    </div>


</div><!-- End .row -->
</div><!-- End .container -->
<?=$this->endSection('content');?>